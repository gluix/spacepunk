package entities;

import com.haxepunk.Entity;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;

import graphics.KeyGraphic;

import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.events.TouchEvent;
import flash.system.Capabilities;
import flash.ui.Multitouch;
import flash.ui.MultitouchInputMode;

/**
* @author McFamily
*/
class TouchKey extends Entity {
	private var PKeyCode:Int;
	private var PTouchPointID:Int;

public function new(x:Int, y:Int, keyCode:Int, imageName:String) {
	super(x, y);
	PKeyCode = keyCode;
	var image:Image = new Image(imageName);
	graphic = image;
	setHitbox(image.width, image.height);

	graphic.scrollX = graphic.scrollY = 0;

	this.x = x;
	this.y = y;
}

public override function added() {

	
	//TouchDevice
	Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
	HXP.stage.addEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
	HXP.stage.addEventListener(TouchEvent.TOUCH_END, onTouchEnd);
	
	//NoTouch / SWF-TEST
	HXP.stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
	HXP.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
}

private function onTouchBegin(event : TouchEvent) {
	if (collidePoint(this.x, this.y, event.stageX, event.stageY)) {
		PTouchPointID = event.touchPointID;
		HXP.stage.dispatchEvent(new KeyboardEvent(KeyboardEvent.KEY_DOWN, false, false, 0, PKeyCode));
	}
}

private function onTouchEnd(event : TouchEvent) {
	if (event.touchPointID == PTouchPointID ) {
		HXP.stage.dispatchEvent(new KeyboardEvent(KeyboardEvent.KEY_UP, false, false, 0, PKeyCode));
	}
}

// for testing
private function onMouseDown(event : MouseEvent) {
	if (collidePoint(this.x, this.y, event.stageX, event.stageY)) {
		HXP.stage.dispatchEvent(new KeyboardEvent(KeyboardEvent.KEY_DOWN, false, false, 0, PKeyCode));
	}
}

private function onMouseUp(event : MouseEvent) {
		// if (collidePoint(this.x, this.y, event.stageX, event.stageY)) {
		HXP.stage.dispatchEvent(new KeyboardEvent(KeyboardEvent.KEY_UP, false, false, 0, PKeyCode));
		// }
	}
}