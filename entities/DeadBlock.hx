package entities;
 
import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import src.GameStore;

class DeadBlock extends Entity
{
	private var _image:Image;
	private static var WIDTH:Int = 32;
	private static var HEIGHT:Int =  32;
	public var _color:Int;
	
	public function new()
	{
		super(-50,-50);
	}
	
    public function setup(myx:Int, myy:Int, color:Int)
    {
		x = myx * 32;
		y = myy * 32;
		layer = 2;
		_color = color;		      
		setColor(color);
		graphic = _image;	
		type = "dead"; 
    }
	
	public function setColor(color:Int)
	{
		_color = color;
        if (_color == 0)
		{
			_image = GameStore.blockBlue;
		}
		else if (_color == 1)
		{
			_image = GameStore.blockGeld;
		}
		else if (_color == 2)
		{
			_image = GameStore.blockGreen;
		}		
		else if (_color == 3)
		{
			_image = GameStore.blockLila;
		}
		else if (_color == 4)
		{
			_image = GameStore.blockGrey;
		}
		graphic = _image;
	}	
}
