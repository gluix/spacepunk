package entities;

import flash.display.BitmapData;
import flash.geom.Point;
import com.haxepunk.Graphic;
/**
* ...
* @author Reiss
*/
class BloomWrapper extends Graphic
{
	public var bloomCanvas:BitmapData;
	private var _graphic:Graphic;

	public function new(g:Graphic)
	{
		super();
		
		bloomCanvas = null;
		_graphic = g;
		active = g.active;
		visible = g.visible;
		relative = g.relative;
	}

	public var wrappedGraphic( getWrappedGraphic, never ) : Graphic;
	public function getWrappedGraphic():Graphic
	{
		return _graphic;
	}

	override public function render(target:BitmapData, point:Point, camera:Point)
	{
		if (bloomCanvas != null)
		{
			_graphic.render(bloomCanvas, point, camera);			
		}
		_graphic.render(target, point, camera);
	}

	override public function update()
	{
		_graphic.update();
	}

}