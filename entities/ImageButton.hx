package entities;

import entities.FullTouch;
import com.haxepunk.graphics.Image;
import com.haxepunk.masks.Pixelmask;
import com.haxepunk.Entity;
import com.haxepunk.HXP;
import flash.external.ExternalInterface;

import graphics.KeyGraphic;

import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.events.TouchEvent;
import flash.system.Capabilities;
import flash.ui.Multitouch;
import flash.ui.MultitouchInputMode;

class ImageButton extends FullTouch
{
	var img:Image;
	
	public function new(imgPath: String,keyCode:Int, posX:Int, posY:Int) 
	{
		img = new Image(ApplicationMain.getAsset(imgPath));
		img.scale = 0.5;
		graphic = img;
		
		mask = new Pixelmask(img);		
		super(keyCode, posX, posY);	
		super.initHitmask(new Pixelmask(img));
		
	}
	
}