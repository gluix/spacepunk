package entities;
 
import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import src.GameStore;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;

class Block extends Entity
{
	private var _image:Image;
	private static var WIDTH:Int = 32;
	private static var HEIGHT:Int =  32;
	public var _color:Int;
	
	public function new()
	{
		super(-50,-50);
	}
	
    public function setup(myx:Int, myy:Int, color:Int)
    {        
		x = myx * 32;
		y = myy * 32;
		layer = 2;
		_color = color;
        if (_color == 0)
		{
			_image = GameStore.blockBlue;
		}
		else if (_color == 1)
		{
			_image = GameStore.blockGeld;
		}
		else if (_color == 2)
		{
			_image = GameStore.blockGreen;
		}		
		else if (_color == 3)
		{
			_image = GameStore.blockLila;
		}
		else if (_color == 4)
		{
			_image = GameStore.blockGrey;
		}
		
		graphic = _image;	
		setHitbox(32,32);
		type = "wall"; 
    }
	
	public override function update()
    {
		
		if (Input.check(Key.LEFT)) {
			moveBy( -src.GameStore.GameSpeed * 60 * 1.5 * HXP.elapsed, 0);
			src.GameStore.nomove = false;
			src.GameStore.timeshock = Math.min(src.GameStore.timeshock + 0.25, 5000);
		} 
		else if (Input.check(Key.RIGHT) && (src.GameStore.timeshock >= 0.5)) {			
			moveBy((src.GameStore.GameSpeed / 2) * 60 * HXP.elapsed, 0);
			src.GameStore.nomove = true;
			src.GameStore.timeshock -= 0.5;			
		}
		else {
			if (src.GameStore.nomove == false)
			{
				moveBy( -src.GameStore.GameSpeed * 60 * HXP.elapsed, 0);
			}
		}
		
		//maximal 10 sekunden zurückspulen => wie weite können die blöcke in 10sekunden maximal nach links fliegen? wenn timeschock = 10 wäre
		var minx:Float = -src.GameStore.GameSpeed * 60 * 1.5 * HXP.elapsed * src.GameStore.timeshock;
		
		if (x < minx)
		{
			HXP.world.recycle(this);
		}
		
		super.update();
		
		/*
		var time:Int = Std.int(GameStore.initacc - data.z * 10);
		
		super.update();
		moveBy(-(src.GameStore.GameSpeed + time) * 60 * HXP.elapsed, 0);
		if (x < 0)
		{
			HXP.world.recycle(this);
		}
		*/
    }
}
