package entities;
 
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;

class Back extends Entity
{
    public function new()
    {
        super(0, 0);
        graphic = new Image(ApplicationMain.getAsset("gfx/back.png"));
		type = "back"; 
    }
	
	public override function update()
    {
        super.update();
    }
}