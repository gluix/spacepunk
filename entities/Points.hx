package entities;
 
import com.haxepunk.Entity;
import com.haxepunk.graphics.Text;

import src.GameStore;

class Points extends Entity
{
    public function new(x:Int, y:Int)
    {
        super(x , y);
		layer = 0;
		collidable = false;
		
		var pointsText:Text = new Text("0", 0, 0, 640, 480);		
		pointsText.color = 0x00ff00;
		pointsText.size = 80;
		graphic = pointsText;
    }
	
	public override function update()
    {	
		src.GameStore.points += 1;
		if ((src.GameStore.points % 15) == 0)
		{			
			cast(graphic, Text).text = Std.string(src.GameStore.points) + " | " + Std.string(src.GameStore.timeshock);			
		}
    }
	
	public function destroy()
	{
		graphic = null;
	}
}