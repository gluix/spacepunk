package entities;

import com.haxepunk.Entity;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.tweens.motion.CircularMotion;
import com.haxepunk.tweens.motion.LinearMotion;
import com.haxepunk.Tween;
import com.haxepunk.utils.Ease;
/**
 * ...
 * @author Reiss
 */
class Particle extends Entity
{
	/* motion and timing constants */
	private static var WIDTH:Int = 64;
	private static var HEIGHT:Int =  64;
	private static var DIAGONAL:Float = HXP.distance(0, WIDTH, WIDTH, 0); //length of the diagonal of the square
	private static var MIN_MOTION_TIME:Float = 4.0;
	private static var MAX_ADDITIONAL_MOTION_TIME:Float = 2.0;
	private static var MIN_ROTATION_TIME:Float = 2.0;
	private static var MAX_ADDITIONAL_ROTATION_TIME:Float = 2.0;
	
	/* motion and image data */
	private var _motion:LinearMotion;
	private var _rotation:CircularMotion;
	private var _image:Image;
	
	public function new() 
	{
		//initialize to a random location at the bottom of the screen
		super(HXP.random * (HXP.width - WIDTH), HXP.height + DIAGONAL);
		
		_image = Image.createRect(WIDTH, HEIGHT, 0x404040);
		graphic = new BloomWrapper(_image);
		
		//move via linear motion tween. when it gets off the screen boundaries, call recycle()
		
		_motion = new LinearMotion(recycle, com.haxepunk.TweenType.Looping);
		_motion.setMotion(x, y, x, -DIAGONAL, MIN_MOTION_TIME + HXP.random*MAX_ADDITIONAL_MOTION_TIME, Ease.cubeIn);
		addTween(_motion);
		
		//rotate via circular motion tween
		_rotation = new CircularMotion(null, com.haxepunk.TweenType.Looping);
		_rotation.setMotion(0, 0, WIDTH/2,0, true, MIN_ROTATION_TIME + HXP.random*MAX_ADDITIONAL_ROTATION_TIME);
		addTween(_rotation);
		
		//center the image origin, so the image rotates around the center of the entity
		_image.centerOO();
	}
	
	override public function update()
	{
		super.update();
		//set the angle and the location
		_image.angle = HXP.angle(0, 0, _rotation.x, _rotation.y);
		y = _motion.y;
	}
	
	//recyclyes the entity, and moves it back to a random location on the bottom of the screen
	public function recycle()
	{
		HXP.world.recycle(this);
		y = 0;
		x = HXP.random * (HXP.width - WIDTH);
	}
}

