package entities;
 
import com.haxepunk.Entity;
import com.haxepunk.graphics.Text;
import src.GameStore;

class TextEntity extends Entity
{	
	public var myText:Text;
	
    public function new(x:Int, y:Int, text:String, size:Int)
    {
        super(x , y);
		myText = new Text(text,0,0);
		myText.color = 0x00ff00;
		myText.size = size;
		graphic = myText;
    }
	
	public override function update()
    {
    }
}