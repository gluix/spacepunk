package entities;

/**
 * ...
 * @author tim.vengels
 */
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.HXP;
import src.GameStore;
import entities.DeadBlock;
import entities.Block;


class ThePlayer extends Entity {
	private var jumpPower:Float;
	private var vFriction:Float;
	private var ySpeed:Float;

	public function new() {
		super(100, 225);
		layer = 1;
		jumpPower = 1;		
		vFriction=0.95;
		ySpeed=0;
		graphic=new Image(ApplicationMain.getAsset("gfx/player_big.png"));
		setHitbox(42,84);
	}
	
	override public function update() {
		var pressed:Bool=false;


		
		if (Input.check(Key.UP)) {
			ySpeed-=jumpPower;
		} 
			
		if (Input.check(Key.DOWN)) {	
			ySpeed+=jumpPower;
		}
				
		ySpeed *= vFriction;
		
		
		
		adjustYPosition();
	}
	
	private function adjustYPosition() {
		
		if (collide("wall", x, y) != null) {
			var b:Block = cast(collide("wall", x, y), Block);
			if (b._color == 4)
			{
				HXP.world = new worlds.GameOver();	
			}
			else {
				if (b._color == GameStore.color || GameStore.color == -1)
				{
					GameStore.color = b._color;
					GameStore.stock += 1;
										
					if (GameStore.stock == 1)
					{
						GameStore.b1.setColor(b._color);
					}
					else if (GameStore.stock == 2)
					{
						GameStore.b2.setColor(b._color);
					}
					else if (GameStore.stock == 3)
					{
						GameStore.b1.setColor(4);
						GameStore.b2.setColor(4);
						GameStore.b3.setColor(4);
						GameStore.color = -1;
						GameStore.stock = 0;
						GameStore.points += 1000;
					}
					
					HXP.world.recycle(b);
				}
				else
				{
					HXP.world = new worlds.GameOver();					
				}			
			}
		}
			
		for (i in 0...Std.int(Math.abs(ySpeed))) {
			//Begrezung Speilfeld-Rand
			var newPos:Float = HXP.sign(ySpeed) + y;
			if (newPos  <= 0 || newPos >= 540-84)
			{
				ySpeed = 0;
			}
			else {
				y+=HXP.sign(ySpeed);
			}
		}
	}
}