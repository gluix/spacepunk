package graphics;

import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Text;
import com.haxepunk.utils.Key;

import flash.display.BitmapData;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

/**
* @author McFamily
*/
class KeyGraphic extends Image {
	public function new(keyCode:Int, color:Int) {
		super(drawImage(keyCode, color), null);
	}

	private function drawImage(keyCode:Int, color:Int) : BitmapData {
		var radius:Int = 40;
		var shape:Sprite = new Sprite();
		var g:Graphics = shape.graphics;
		g.beginFill(color);
		g.drawCircle(radius, radius, radius);
		g.endFill();

		var text:TextField = new flash.text.TextField();
		text.defaultTextFormat = new flash.text.TextFormat("default", 72);
		text.text = Key.nameOfKey(keyCode);
		text.embedFonts = true;
		text.autoSize = TextFieldAutoSize.LEFT;
		text.x = 2 + (shape.width - text.textWidth) * 0.5;
		text.y = 2 + (shape.height - text.textHeight) * 0.5;
		shape.addChild(text);

		var bitmapData : BitmapData = new BitmapData(radius * 2, radius * 2, true, 0x00FFFFFF);
		bitmapData.draw(shape, null, null, null, null, true);

		return bitmapData;
	}
}