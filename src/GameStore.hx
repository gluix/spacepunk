package src;
import entities.ThePlayer;
import entities.DeadBlock;
import com.haxepunk.graphics.Image;

class GameStore
{
	static public var GameSpeed:Int = 5;
	static public var points:Int = 0;
	
	static public var blockGeld:Image;
	static public var blockGreen:Image;
	static public var blockBlue:Image;
	static public var blockLila:Image;
	static public var blockGrey:Image;
	
	static public var initacc:Float = -1;
	static public var timeshock:Float = 0;
	static public var stock:Int = 0;
	static public var color:Int = -1;
	static public var nomove:Bool = false;
	
	static public var b1:DeadBlock;
	static public var b2:DeadBlock;
	static public var b3:DeadBlock;
	
	//static public var blockGeld:Image = new Image(ApplicationMain.getAsset("gfx/block2_gelb.png"));	
	//static public var blockGreen:Image = new Image(ApplicationMain.getAsset("gfx/block2_green.png"));
	//static public var blockBlue:Image = new Image(ApplicationMain.getAsset("gfx/block2_blau.png"));
	//static public var blockLila:Image = new Image(ApplicationMain.getAsset("gfx/block2_lila.png"));
}