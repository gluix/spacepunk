import com.haxepunk.Engine;
import com.haxepunk.HXP;
import flash.ui.Multitouch;
import flash.ui.MultitouchInputMode;
import flash.events.TouchEvent;
import worlds.GameWorld;
import worlds.GameMenu;
import com.haxepunk.Sfx;

class Main extends Engine
{
	public static inline var kScreenWidth:Int = 960;
	public static inline var kScreenHeight:Int = 540;
	public static inline var kFrameRate:Int = 60;
	public static inline var kClearColor:Int = 0x000000;
	public static inline var kProjectName:String = "HaxePunk";

	public function new()
	{
		super(kScreenWidth, kScreenHeight, kFrameRate, false);
	}

	override public function init()
	{
#if debug
	#if flash
		if (flash.system.Capabilities.isDebugger)
	#end
		{
			HXP.console.enable();
		}
#end
		HXP.console.enable();
		//HXP.screen.color = kClearColor;
		//HXP.screen.scale = 1;
		
		var music:Sfx = new Sfx(ApplicationMain.getAsset('music/darkseid.mp3'));
		music.loop();
		
		HXP.world = new GameMenu();
		Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
	}

	public static function main()
	{
		new Main();
	}
}