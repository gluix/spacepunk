package worlds;

import com.haxepunk.HXP;
import com.haxepunk.World;
import com.haxepunk.graphics.Text;
import com.haxepunk.Entity;
import com.haxepunk.masks.Pixelmask;
import com.haxepunk.utils.Key;
import com.haxepunk.utils.Input;
import entities.Block;
import entities.FullTouch;
import entities.ThePlayer;
import entities.TouchKey;
import src.GameStore;
import worlds.BloomWorld;
import com.haxepunk.Sfx;

class GameOver extends worlds.BloomWorld  
{	
	public function new() 
	{
		super();
		HXP.screen.color = 0x000000;
	}
	
	public override function begin()
	{
		super.begin();
		
		var oldPoints:Int = src.GameStore.points;
		src.GameStore.points = 0;
		src.GameStore.color = -1;
		src.GameStore.stock = 0;
		src.GameStore.timeshock = 0;
		
		//neue runde wieder langsam starten
		src.GameStore.GameSpeed = 5;
					
		var splashText:Text = new Text("GAME OVER!", 0,0,640,480);
		splashText.color = 0x00ff00;
		splashText.size = 55;
		var splashEntity:Entity = new Entity(0, 0, splashText);
		splashEntity.x = (HXP.width/2)-(splashText.width/2);
		splashEntity.y = splashText.height + 30;
		splashEntity.layer = 0;
		add(splashEntity);
		
		var pointsText:Text = new Text(Std.string(oldPoints), 0,0,640,480);
		pointsText.color = 0x00ff00;
		pointsText.size = 120;	
		var pointsEntity:Entity = new Entity(0,0,pointsText);
		pointsEntity.x = (HXP.width/2)-(pointsText.width/2);
		pointsEntity.y = splashEntity.y + splashText.height + 20;
		pointsEntity.layer = 0;
		add(pointsEntity);
				
		var titleText:Text = new Text("Let's Play again!", 0,0,640,480);
		titleText.color = 0xffffff;
		titleText.size = 55;
		titleText.angle = 5;
		var textEntity:Entity = new Entity(0,0,titleText);
		textEntity.x = (HXP.width/2)-(titleText.width/2);
		textEntity.y = pointsEntity.y + pointsText.height + 30;
		textEntity.layer = 0;
		add(textEntity);
		
		var touch:FullTouch = new entities.FullTouch(Key.X, Std.int(textEntity.x), Std.int(textEntity.y));
		touch.initHitmask(new Pixelmask(textEntity.graphic));
		add(touch);	
		
		var menuText:Text = new Text("Menu !", 0,0,640,480);
		menuText.color = 0xffffff;
		menuText.size = 55;
		menuText.angle = 5;
		var menuEntity:Entity = new Entity(0,0,menuText);
		menuEntity.x = (HXP.width/2)-(menuText.width/2);
		menuEntity.y = textEntity.y + titleText.height + 30;
		menuEntity.layer = 0;
		add(menuEntity);
		
		
		
	}	
	
	public override function update() 
	{
		super.update();
		
		if (Input.check(Key.X)) 
		{			
			var music:Sfx = new Sfx(ApplicationMain.getAsset('sfx/teleport.wav'));
			music.play();
			HXP.world=new GameWorld();
		}
	}
}
