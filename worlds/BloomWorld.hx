package worlds;

import com.haxepunk.HXP;
import com.haxepunk.World;
import com.haxepunk.graphics.Text;
import com.haxepunk.Entity;
import com.haxepunk.utils.Key;
import com.haxepunk.utils.Input;
import entities.BloomLighting;
import entities.BloomWrapper;
import entities.Particle;

/**
* ...
* @author Reiss
*/
class BloomWorld extends World
{
	/* time between particle creation */
	public static var MIN_LAPSED_CREATE_TIME:Float = 0.5;
	public static var MAX_ADDITIONAL_CREATE_TIME:Float = 0.025;

	/* amount of bloom and quality of bloom desired */
	private static var BLOOM:Float = 20.0;
	private static var BLOOM_QUALITY:Int = 1;

	/* particle timing */
	private var _elapsed:Float;
	private var _timeToNextParticle:Float;

	/* bloom effect */
	private var _bloom:BloomLighting;

	/* data */
	private var _wrappedGraphic:BloomWrapper;

	public function new() 
	{
		super();
		_elapsed = 0.0;
		_timeToNextParticle = calcNextParticleTime();
		_bloom = new BloomLighting(BLOOM, BLOOM_QUALITY);
	}

	override public function begin()
	{
		HXP.console.enable();

		//set the layer and color of the bloom, and add it to the world
		_bloom.layer = -1;
		_bloom.color = 0xafffff;
		add(_bloom);

		//create, add, and register an initial particle
		//_bloom.register(create(Particle).graphic as BloomWrapper);
		_bloom.register(cast(create(Particle).graphic, BloomWrapper));
	}

	override public function update()
	{
		super.update();

		/* spawn new particles if enough time has passed */
		if (_elapsed >= _timeToNextParticle)
		{
			_elapsed -= _timeToNextParticle;
			_timeToNextParticle = calcNextParticleTime();
			//_bloom.register(create(Particle).graphic as BloomWrapper);
			_bloom.register(cast(create(Particle).graphic, BloomWrapper));
		}
		else
		{
			_elapsed += HXP.elapsed;
		}
	}

	/* returns a random amount of time to wait before spawning a new particle */
	private function calcNextParticleTime():Float
	{
		return MIN_LAPSED_CREATE_TIME + (HXP.random * MAX_ADDITIONAL_CREATE_TIME);
	}
}