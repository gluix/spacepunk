package worlds;

import com.haxepunk.HXP;
import com.haxepunk.World;
import com.haxepunk.graphics.Text;
import com.haxepunk.Entity;
import com.haxepunk.utils.Key;
import com.haxepunk.utils.Input;
import com.haxepunk.masks.Pixelmask;
import entities.Block;
import entities.TextEntity;
import entities.ThePlayer;
import entities.TouchKey;
import entities.FullTouch;
import worlds.BloomWorld;
import com.haxepunk.Sfx;

class GameMenu extends worlds.BloomWorld 
{	
	public function new() 
	{
		super();
		HXP.screen.color = 0x000000;	
	}
	
	public override function begin()
	{
		super.begin();
		//add(new entities.FullTouch(Key.X, HXP.width, HXP.height, 0, 0));
		
				
		// Text input
		/*
		var nameInputBox:TextField = new TextField();
		var formatNameInputbox:TextFormat = new TextFormat();

		nameInputBox.type = TextFieldType.INPUT;
		formatNameInputbox.size = 24;
		nameInputBox.defaultTextFormat = formatNameInputbox;
_		nameInputBox.text = "Tim";
		*/

				//sound
		/*
		music = new Sfx(MUSIC)
		music.volume = 0.5;
		music.loop(); 
		*/	
		
		var splashText:Text = new Text("HelloPunk!", 0,0,640,480);
		splashText.color = 0x00ff00;
		splashText.size = 120;
		splashText.angle = 15;
		splashText.centerOO();
		var splashEntity:Entity = new Entity(0, 0, splashText);
		splashEntity.x = (HXP.width/2)-(splashText.width/4);
		splashEntity.y = (HXP.height / 2) - (splashText.height);
		splashEntity.layer = 0;
		add(splashEntity);
				
		var titleText:Text = new Text("Let's Play !", 0,0,640,480);
		titleText.color = 0xffffff;
		titleText.size = 55;
		titleText.angle = 5;
		var textEntity:Entity = new Entity(0,0,titleText);
		textEntity.x = (HXP.width/2)-(titleText.width/2);
		textEntity.y = (HXP.height / 2) - (titleText.height / 2);
		textEntity.layer = 0;
		add(textEntity);
		
		var touch:FullTouch = new entities.FullTouch(Key.X, Std.int(textEntity.x), Std.int(textEntity.y));
		touch.initHitmask(new Pixelmask(HXP.getBitmap(titleText)));
		add(touch);	
		
		var menuText:Text = new Text("Settings !", 0,0,640,480);
		menuText.color = 0xffffff;
		menuText.size = 55;
		menuText.angle = 5;
		var menuEntity:Entity = new Entity(0,0,menuText);
		menuEntity.x = (HXP.width/2)-(menuText.width/2);
		menuEntity.y = (HXP.height / 2) - (menuText.height / 2) + titleText.height + 40;
		menuEntity.layer = 0;
		add(menuEntity);
	}	
	
	public override function update() 
	{
		super.update();
		if (Input.check(Key.X)) 
		{					
			//var music:Sfx = new Sfx(ApplicationMain.getAsset('sfx/flagreturn.wav'));
			//music.play();
			HXP.world=new GameWorld();
		}
	}
}
