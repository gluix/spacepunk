package worlds;
 
import com.haxepunk.HXP;
import com.haxepunk.World;
import com.haxepunk.graphics.Image;
import com.haxepunk.Entity;
import com.haxepunk.utils.Key;
import entities.Block;
import entities.ImageButton;
import entities.FullTouch;
import entities.Points;
import entities.ThePlayer;
import entities.TouchKey;
import src.GameStore;
import entities.Back;
import entities.DeadBlock;


class GameWorld extends World
{	
	private var lastSet:Int;
	public var gameUpdate:Int;
	public var minY:Int;
	public var maxY:Int;
	public var marginY:Int;

		
    public function new()
    {
        super();
		minY = 16;
		maxY = 0;
		marginY = 3;
		gameUpdate = 0;
		lastSet = -1;
		HXP.screen.color = 0x000000;
    }
    
    public override function begin()
	{		
		GameStore.blockGeld = new Image(ApplicationMain.getAsset("gfx/block2_gelb.png"));	
		GameStore.blockGreen= new Image(ApplicationMain.getAsset("gfx/block2_green.png"));
		GameStore.blockBlue = new Image(ApplicationMain.getAsset("gfx/block2_blau.png"));
		GameStore.blockLila = new Image(ApplicationMain.getAsset("gfx/block2_lila.png"));
		GameStore.blockGrey = new Image(ApplicationMain.getAsset("gfx/block2_grey.png"));
		
		add(new Back());
		
		
		
		GameStore.b1 = cast(HXP.world.create(DeadBlock), DeadBlock);
		GameStore.b1.setup(25,1,4);
		add(GameStore.b1);
		
		GameStore.b2 = cast(HXP.world.create(DeadBlock), DeadBlock);
		GameStore.b2.setup(26,1,4);
		add(GameStore.b2);
		
		GameStore.b3 = cast(HXP.world.create(DeadBlock), DeadBlock);
		GameStore.b3.setup(27,1,4);
		add(GameStore.b3);
		
			
		add(new ThePlayer());	
		
		
		//add(new FullTouch(Key.UP, HXP.width, Std.int(HXP.height / 2),0,0));
		//add(new FullTouch(Key.DOWN, HXP.width, Std.int(HXP.height / 2), 0, Std.int(HXP.height / 2)));
		//add(new ImageButton("gfx/button-up.png", Key.UP, 0, Std.int(((HXP.height / 2) - 128) / 2);
		//add(new ImageButton("gfx/button-down.png", Key.DOWN, 0, Std.int(((HXP.height / 2) - 128) / 2);
		
		var freeSpace:Int = HXP.height - 2 * 128;
		var margin:Int = Std.int(freeSpace / 4);
		
		var up:ImageButton = new ImageButton("gfx/button-up.png", Key.UP, 0, margin);
		up.setHitbox(Std.int(HXP.width / 2), Std.int(HXP.height / 2));
		add(up);
		
		var down:ImageButton = new ImageButton("gfx/button-down.png", Key.DOWN, 0, Std.int((HXP.height / 2) + margin));
		down.setHitbox(Std.int(HXP.width / 2), Std.int(HXP.height / 2));
		add(down);
		
		var left:ImageButton = new ImageButton("gfx/button-left.png", Key.LEFT, HXP.width - 128 - 20, margin);
		left.setHitbox(Std.int(HXP.width / 2), Std.int(HXP.height / 2));
		add(left);
		
		var right:ImageButton = new ImageButton("gfx/button-right.png", Key.RIGHT, HXP.width - 128 - 20, Std.int((HXP.height / 2) + margin));
		right.setHitbox(Std.int(HXP.width / 2), Std.int(HXP.height / 2));
		add(right);
		
		add(new Points(5, 5));
	}
	
	public override function update()
	{
		
		
		
		//adding block		
		gameUpdate = gameUpdate + 1;
		
		if (gameUpdate == 750)
		{
			src.GameStore.GameSpeed += 1;			
			gameUpdate = 0;			
		}
		
		//make games faster
		var blockList:Array<com.haxepunk.Entity> = [];

		// Then, we populate the array with all existing Enemy objects!
		getClass(Block, blockList);

		var maxPos:Int = 0;
		for ( i in 0...blockList.length ) {
			maxPos = Std.int(Math.max(cast(blockList[i], Block).x, maxPos));
		}
		
		if (maxPos <= (25 * 32))
		{
			HXP.randomizeSeed();
			var set:Int = HXP.rand(6);
			var color:Int = 4;
			var direction:Int = HXP.rand(2);
			var item:Int = HXP.rand(3);
			//var yPos:Int = HXP.rand(16);
			var yPos:Int;
			
			//sorgt dafür, dass immer mindestens 5Blöcke Abstand zum Vorgänger eingehalten werden (damit das Level immer möglich ist)
			minY = minY - marginY;
			maxY = maxY + marginY;
			
			if (direction == 0 && minY > 0)
			{
				yPos = HXP.rand(minY); //von 0 - minY
			}
			else if (maxY < 16)
			{
				
				yPos = maxY + HXP.rand(16 - maxY); //von maxY - 16 (es gibt nur 16 Blöcke auf dem Screen)
			}
			else 
			{
				yPos = HXP.rand(16);
			}
			
			if (item == 1)
			{
				color = HXP.rand(4);
			}
			
			
			//var yPos:Int = HXP.rand(16);
			
			
			//yPos = Std.int(Math.min(minY - marginY, yPos));
			//yPos = Std.int(Math.max(maxY + marginY, yPos));

			
			
			/*
			while (set == lastSet)
			{
				set = HXP.rand(6);
			}
			*/
			
			var b:Block;
			if (set == 0)
			{
				for (i in 1...5 ) {		
					b = cast(HXP.world.create(Block), Block);
					b.setup(30,yPos-i,color);
					add(b);
				}
				maxY = yPos;
				minY = yPos - 5;
			}
			else if (set == 1)
			{
				for (i in 1...5 ) {
					b = cast(HXP.world.create(Block), Block);
					b.setup(30+i,yPos-4-i,color);
					add(b);
					b = cast(HXP.world.create(Block), Block);
					b.setup(30+i,yPos-i,color);
					add(b);
				}
				maxY = yPos - 5;
				minY = yPos - 5 - 4;
			}
			else if (set == 2)
			{
				for (i in 1...7 ) {
					b = cast(HXP.world.create(Block), Block);
					b.setup(30+i,yPos-8,color);
					add(b);
					b = cast(HXP.world.create(Block), Block);
					b.setup(30 + i, yPos, color);
					add(b);
				}
				maxY = yPos;
				minY = yPos - 8;
			}
			else if (set == 3)
			{
				for (i in 1...6 ) {
					b = cast(HXP.world.create(Block), Block);
					b.setup(30 + i, yPos, color);
					add(b);
				}	
				maxY = yPos;
				minY = yPos;
			}
			else if (set == 4)
			{
				for (i in 1...10 ) {	
					b = cast(HXP.world.create(Block), Block);
					b.setup(30 + 15 - i, yPos, color);
					add(b);
				}
				maxY = yPos;
				minY = yPos;
			}
			else if (set == 5)
			{
				for (i in 1...5 ) {
					b = cast(HXP.world.create(Block), Block);
					b.setup(30 + i, yPos + i, color);
					add(b);
				}
				for (i in 1...5 ) {
					b = cast(HXP.world.create(Block), Block);
					b.setup(35 + i, yPos - i, color);
					add(b);
				}
				maxY = yPos + 5;
				minY = yPos - 5;
			}
			
			lastSet = set;
		}
		
		super.update();
	}
}